export async function wait(milli) {
  await new Promise(resolve => setTimeout(resolve, milli));
}