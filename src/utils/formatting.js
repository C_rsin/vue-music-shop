export function combineArtistsToString(artists) {
  return artists.reduce((acc, artist, index, artists) => {
    acc += artist.name;
    if (index < artists.length - 1) {
      acc += ", ";
    }
    return acc;
  }, "");
}