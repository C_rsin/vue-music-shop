import { Track, Artist } from '@/model';



export default [
  new Track({
    id: 1,
    name: 'SLVR',
    price: '1.99',
    image: 'slvr.webp',
    color: '#E3333B',
    artists: [
      new Artist({
        id: 1,
        name: 'Steve Angello'
      }),
      new Artist({
        id: 2,
        name: 'Matisse & Sadko'
      })
    ]
  }),

  new Track({
    id: 2,
    name: 'Consequences',
    price: '1.99',
    image: 'consequences.png',
    artists: [
      new Artist({
        id: 3,
        name: 'JEBU'
      })
    ]
  }),

  new Track({
    id: 3,
    name: 'Hanover',
    price: '1.99',
    image: 'hanover.webp',
    color: '#7987A5',
    artists: [
      new Artist({
        id: 4,
        name: 'Jeremy Olander'
      })
    ]
  }),

  new Track({
    id: 4,
    name: 'Bubblegum',
    price: '1.99',
    image: 'bubblegum.webp',
    color: '#DF8163',
    artists: [
      new Artist({
        id: 5,
        name: 'Monte'
      })
    ]
  }),

  new Track({
    id: 5,
    name: 'In The Flood',
    price: '1.99',
    image: 'in-the-flood.webp',
    artists: [
      new Artist({
        id: 6,
        name: 'Oliver Winters'
      })
    ]
  }),

  new Track({
    id: 6,
    name: 'Paris (Andhim Remix)',
    price: '1.99',
    image: 'paris.webp',
    artists: [
      new Artist({
        id: 7,
        name: 'Groove Armada'
      }),
      new Artist({
        id: 8,
        name: 'Andhim'
      })
    ]
  }),

  new Track({
    id: 7,
    name: 'Make \'Em Dance',
    price: '1.99',
    image: 'make-em-dance.webp',
    color: '#BD8B5D',
    artists: [
      new Artist({
        id: 9,
        name: 'CamelPhat'
      })
    ]
  }),

  new Track({
    id: 8,
    name: 'Letters (Capital) (Jeremy Olander Remix)',
    price: '1.99',
    image: 'letters.webp',
    color: '#65686C',
    artists: [
      new Artist({
        id: 10,
        name: 'Eekkoo'
      }),
      new Artist({
        id: 11,
        name: 'Sailor & I'
      }),
      new Artist({
        id: 4,
        name: 'Jeremy Olander'
      })
    ]
  }),

  new Track({
    id: 9,
    name: 'Skin and Bones (Enamour Remix)',
    price: '1.99',
    image: 'skin-and-bones.webp',
    color: '#979DA9',
    artists: [
      new Artist({
        id: 12,
        name: 'Lane 8'
      }),
      new Artist({
        id: 13,
        name: 'Patrick Baker'
      }),
      new Artist({
        id: 14,
        name: 'Enamour'
      })
    ]
  }),

  new Track({
    id: 10,
    name: 'Soleil',
    price: '1.99',
    image: 'soleil.webp',
    color: '#27658F',
    artists: [
      new Artist({
        id: 15,
        name: 'Sebjak'
      }),
      new Artist({
        id: 16,
        name: 'David Pietras'
      })
    ]
  }),

  new Track({
    id: 11,
    name: 'Last Dance',
    price: '1.99',
    image: 'last-dance.webp',
    color: '#70DA75',
    artists: [
      new Artist({
        id: 17,
        name: 'Christoph'
      }),
      new Artist({
        id: 4,
        name: 'Jeremy Olander'
      })
    ]
  }),

  new Track({
    id: 12,
    name: 'Coffee Clouds (Andhim Remix)',
    price: '1.99',
    image: 'coffee-clouds.webp',
    color: '#8CA3B0',
    artists: [
      new Artist({
        id: 200,
        name: 'Matthias Tanzmann'
      }),
      new Artist({
        id: 8,
        name: 'Andhim'
      })
    ]
  }),

  new Track({
    id: 13,
    name: 'Underworld (Undercatt Remix)',
    price: '1.99',
    image: 'underworld.webp',
    color: '#8B5445',
    artists: [
      new Artist({
        id: 18,
        name: 'Edu Imbernon'
      }),
      new Artist({
        id: 19,
        name: 'Duologue'
      }),
      new Artist({
        id: 20,
        name: 'Undercatt'
      })
    ]
  }),

  new Track({
    id: 14,
    name: 'Parfume',
    price: '1.99',
    image: 'parfume.webp',
    color: '#A5A949',
    artists: [
      new Artist({
        id: 21,
        name: 'Eli & Fur'
      })
    ]
  }),

  new Track({
    id: 15,
    name: 'As I Sleep',
    price: '1.99',
    image: 'as-i-sleep.webp',
    color: '#55A6DB',
    artists: [
      new Artist({
        id: 22,
        name: 'Adrian Lux'
      }),
      new Artist({
        id: 23,
        name: 'Tobtok'
      }),
      new Artist({
        id: 24,
        name: 'Charlee'
      })
    ]
  }),

  new Track({
    id: 16,
    name: 'Shine',
    price: '1.99',
    image: 'shine.webp',
    color: '#655E8C',
    artists: [
      new Artist({
        id: 25,
        name: 'Super Flu'
      })
    ]
  }),
]