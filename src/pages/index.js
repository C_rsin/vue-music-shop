import ShopBrowserPage from './shop-browser/shop-browser-page';
import CustomerPage from './customer/customer-page';
import PageNotFoundPage from './error/page-not-found-page';
import ShopItemPage from './shop-item/shop-item-page';
import CheckoutPage from './checkout/checkout-page';

export {
  ShopBrowserPage,
  ShopItemPage,
  CustomerPage,
  CheckoutPage,
  PageNotFoundPage
}
