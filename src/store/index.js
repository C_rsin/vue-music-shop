import Vue from 'vue';
import Vuex from 'vuex';

import cart from './modules/cart';
import shop from './modules/shop';
import wishList from './modules/wish-list';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    cart,
    shop,
    wishList
  }
});

export default store;