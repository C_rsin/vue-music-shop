export default {
  name: 'wishList',
  namespaced: true,
  state: {
    wishList: []
  },
  getters: {
    wishList: state => state.wishList
  },
  mutations: {
    add(state, item) {
      state.wishList.push(item);
    },
    remove(state, itemId) {
      state.wishList = state.wishList.filter(item => item.id !== itemId);
    }
  },
  actions: {
    add({ commit }, item) {
      commit('add', item);
    },
    remove({ commit }, itemId) {
      commit('remove', itemId);
    }
  }
}
