import { Track } from '@/model';

import items from '@/data/shop-items';

export default {
  name: 'shop',
  namespaced: true,
  state: {
    items: [],
    currentItem: Track.buildDefault()
  },
  getters: {
    items: state => state.items,
    currentItem: state => state.currentItem
  },
  mutations: {
    setItems(state, items) {
      state.items = items;
    },
    setCurrentItem(state, item) {
      state.currentItem = item;
    }
  },
  actions: {
    async fetchItems({ commit }) {
      commit('setItems', items);
    },
    async findById({ commit, state, dispatch }, id) {
      if (state.items.length < 1) {
        await dispatch('fetchItems');
      }
      commit('setCurrentItem', state.items.find(item => item.id === id));
    }
  }
}