export default {
  name: 'cart',
  namespaced: true,
  state: {
    cart: []
  },
  getters: {
    cart: state => state.cart
  },
  mutations: {
    add(state, item) {
      if (!state.cart.find(i => i.id === item.id)) {
        state.cart.push(item);
      }
    },
    remove(state, itemId) {
      state.cart = state.cart.filter(item => item.id !== itemId);
    }
  },
  actions: {
    add({ commit, dispatch }, item) {
      commit('add', item);
      dispatch('wishList/remove', item.id, { root: true });
    },
    remove({ commit }, itemId) {
      commit('remove', itemId);
    }
  }
}
