export class Track {

  constructor(item) {
    this.id = item.id;
    this.price = item.price;
    this.name = item.name;
    this.image = item.image;
    this.color = item.color;
    this.artists = item.artists;
  }

  static buildDefault() {
    return new Track({
      id: -1,
      price: '0.00',
      name: 'Track',
      image: 'slvr.webp',
      artists: []
    });
  }

}