export class Artist {
  constructor(artist) {
    this.id = artist.id;
    this.name = artist.name;
  }

  static buildDefault() {
    return new Artist({
      id: -1,
      name: 'Artist'
    });
  }
}