import Vue from 'vue'
import Router from 'vue-router'

import {
  ShopBrowserPage,
  ShopItemPage,
  CustomerPage,
  CheckoutPage,
  PageNotFoundPage
} from '@/pages';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'root',
      redirect: {
        name: 'shop'
      }
    },
    {
      path: '/shop',
      name: 'shop',
      component: ShopBrowserPage
    },
    {
      path: '/item/:id',
      name: 'item',
      component: ShopItemPage
    },
    {
      path: '/customer',
      name: 'customer',
      component: CustomerPage,
      redirect: {
        name: 'cart'
      },
      children: [
        {
          path: '/cart',
          name: 'cart'
        },
        {
          path: '/wish-list',
          name: 'wish-list'
        }
      ]
    },
    {
      path: '/checkout',
      name: 'checkout',
      component: CheckoutPage
    },
    {
      path: '*',
      name: 'not-found',
      component: PageNotFoundPage
    }
  ]
})
