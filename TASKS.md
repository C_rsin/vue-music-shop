# Tasks

#### 1. Displaying data
1.1 Display variable content \
1.2 Render computed property

_in file src/components/shop-item/**shop-item.vue**_

####2. Conditional rendering

_in file src/components/pages/template/cart-button/**cart-button.vue**_

####3. Binding
3.1 Property-Binding \
3.2 Event-Binding 

_in file src/components/button/**go-to-shop-button.vue**_

####4. Loop rendering & Custom events

_in file src/pages/customer/\_components/**wish-list.vue**_

_in file src/components/shop-item/**shop-item.vue**_

####5. Form handling and custom Check-Box Component

_in file src/pages/checkout/**checkout-page.vue**_

_in file src/components/form/**check-box.vue**_

####6. Routing

####7. Vuex
7.1 State, Getters, Mutations, Actions \
7.2 mapGetters, mapActions

####8. HTTP Requests


